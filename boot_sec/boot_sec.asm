;
; A boot sector that prints a string using a function
;
[org 0x7c00]  			; Tell the assembler where this code will be loaded

  mov bx, HELLO_MSG		; Use BX as a paramater to the print function, so
  call print_string		; we can specify the address of a string

  mov bx, GOODBYE_MSG		; Use BX as a paramater to the print function, so
  call print_string		; we can specify the address of a string

  mov dx, 0xfcbf
  call print_hex

  jmp $					; Hang

; Data
HELLO_MSG:
  db 'Hello, World! ',0 ; The zero on the end tells the routine when to
						            ; When to stop printing characters

GOODBYE_MSG: db 'Goodbye! ',0

%include "boot_sec/print.asm"

; Padding and magic number
times 510-($-$$) db 0
dw 0xaa55
