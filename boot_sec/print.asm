print_string:
  pusha         ; push old values onto the stack

  mov ah, 0x0e  ; int 10/ah = 0eh -> scrolling teletype BIOS routine

  ; jmp print_str_loop      ; loop through string

print_str_loop:

  mov al, [bx]  ; move the value at bx into al

  cmp al, 0     ; if it is the null character 0
  je return     ; jump to return

  int 0x10      ; make the interrupt 

  inc bx        ; increment, move to the next character in array

  jmp print_str_loop      ; jump to loop

return:
  popa          ; return the values from the stack back to where they were
  ret           ; return from which we were called

; printing hex numbers
; expects hex to be in dx register

; Improved solution from:
; https://stackoverflow.com/questions/37898693/why-does-this-assembly-language-program-print-hex-digits-in-reverse-order

print_hex:
  pusha

  mov bx, HEX_OUT
  add bx, 2
  mov cl, 12

  print_hex_loop:

    mov ax, dx
    shr ax, cl 
    and al, 0x0f
    add al, 0x30
    cmp al, 0x39
    jle is_digit
    add al, 0x27
    is_digit:
      mov byte [bx], al
      inc bx
      sub cl, 4
      jnb print_hex_loop

  print_hex_str:
    mov bx, HEX_OUT
    call print_string
    jmp return

HEX_OUT: db '0x0000', 0 
