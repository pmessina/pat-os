# Pat OS

This project is me learning operating systems.


# References

The following is references I am using to build the operating system.

* https://www.cs.bham.ac.uk/~exr/lectures/opsys/10_11/lectures/os-dev.pdf
* https://c9x.me/x86/
* http://unixwiz.net/techtips/x86-jumps.html
